import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import { MdChevronLeft } from 'react-icons/md';

import { Form, Input } from '@rocketseat/unform';

// import Avatar from './Avatar';
import { Container, TopHeader } from './styles';
import { updateBrokerRequest } from '../../../store/modules/corretor/actions';

export default function Detail() {
   const dispatch = useDispatch();
   const broker = useSelector(state => state.broker.broker);


  function handleSubmit(data){
     dispatch(updateBrokerRequest(data));
  }
  
  return (
    <Container>
      <TopHeader>
          <Link to='/clientes'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
      </TopHeader>

        <header>
            <strong>Detalhes do Cliente</strong>
        </header>
        <Form initialData={broker} onSubmit={handleSubmit}>

          {/* <Avatar name="avatar_id" /> */}
          
          <Input type="text" name="id" placeholder="Id" style={{display: 'none'}} />
          <Input type="text" name="name" placeholder="Nome" />
          <Input type="email" name="email" placeholder="E-mail" />
          <Input name="phone" placeholder="Telefone" />
          <Input name="doc" placeholder="CPF" />
          <Input name="obs" placeholder="Observações" />
          
          <button type="submit">Atualizar</button>
        </Form>
       
    </Container>
  );
}
