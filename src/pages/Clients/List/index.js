import React, { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

import api from '../../../services/api';

import { Container, TopHeader, Card, Left, Right } from './styles';
import { useDispatch } from 'react-redux';
import { signUpDetail } from '../../../store/modules/cliente/actions';

export default function List() {
    const [clientes, setClientes] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        async function listClientes() {
            const response = await api.get('clientes');

            const data = response.data.map(cliente => ({
                ...cliente
            }))
            setClientes(data);
        }
        listClientes();
    }, []);

    function handleDetail(data) {
        dispatch(signUpDetail(data));
    }

    return (
        <Container>
            <TopHeader>
                <Link to='/cliente'>+ NOVO CLIENTE</Link>
            </TopHeader>

            <header>
                <strong>Clientes</strong>
            </header>

            <ul>
                {clientes.map(cliente => (
                    <Link key={cliente.id} to='/detalhedocliente' onClick={() => handleDetail(cliente)}>
                        <Card>
                            <Left>
                                {/* <img src={(user.avatar && user.avatar.url) || DefaultUser} alt="Avatar"/> */}

    
                            </Left>
                            <Right>
                                <strong>{cliente.name}</strong>
                                <span>{cliente.phone}</span>
                                <span>{cliente.email}</span>                        
                            </Right>
                        </Card>
                    </Link>
                ))}
                {/* <Link to='/detalhedocliente'>
                <Card>
                    <Left>
                        <img src={DefaultUser} alt="Priscila Ribeiro"/>
                    </Left>
                    <Right>
                        <strong>Priscila Neu</strong>
                        <span>Fone: 51 9 9999-9999</span>
                    </Right>
                </Card>
            </Link> */}
            </ul>
        </Container>
    );
}
