import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input } from '@rocketseat/unform';

import { updateProfileRequest } from '../../store/modules/user/actions';

// import Avatar from './Avatar';
import { Container } from './styles';

export default function Profile() {
  const dispatch = useDispatch();
  const profile = useSelector(state => state.user.profile);

  const checkboxOptions = [
    { value: '1', label: 'Inativo' },
  ];


  function handleSubmit(data){
    dispatch(updateProfileRequest(data));
  }

  return (
    <Container>
      <Form initialData={profile} onSubmit={handleSubmit}>
      {/* <Form> */}

        {/* <Avatar name="avatar_id" /> */}
        
        <Input type="text" name="id" placeholder="Id" style={{display: 'none'}} />
        <Input name="name" placeholder="Nome" />
        <Input type="email" name="email" placeholder="E-mail" />
        <Input name="phone" placeholder="Telefone" />
        <Input name="doc" placeholder="CPF" />
        <Input name="obs" placeholder="Observações" />
        {/* <Checkbox name="checkbox" options={checkboxOptions} /> */}

        <hr />

        <Input type="password" name="oldPassword" placeholder="Senha Atual" />
        <Input type="password" name="password" placeholder="Nova Senha" />
        <Input type="password" name="confirmPassword" placeholder="Confirmar Senha" />
        
        <button type="submit">Atualizar Perfil</button>
      </Form>
    </Container>
  );
}
