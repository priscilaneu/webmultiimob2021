import React from 'react';
import { useDispatch } from 'react-redux';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';

import { Link } from 'react-router-dom';
import { MdChevronLeft } from 'react-icons/md';
import { signUpRequest } from '../../store/modules/award/actions';

import { Container, TopHeader } from './styles';
import Placeholder from './Detail/Placeholder';

export default function Premio() {
  const dispatch = useDispatch();
  // const profile = useSelector(state => state.user.profile);

  const schema = Yup.object().shape({
    title: Yup.string()
      .required('O Título é obrigatório'),
      local: Yup.string().required('O Local é obrigatório'),
      number_points: Yup.number().required('A Quantidade de Pontos é obrigatória'),
      site: Yup.string().required('O Site é obrigatória'),
  })

  function handleSubmit(data){
    dispatch(signUpRequest(data));
  }

  return (
    <Container>
      <TopHeader>
          <Link to='/premios'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
      </TopHeader>

      <header>
          <strong>Cadastrar nova Premiação</strong>
      </header>

      <Form schema={schema} onSubmit={handleSubmit}>

        <label>Imagem de Destaque</label>
        <Placeholder name="featured_image_id" />
        
        <label>Incluir mais Imagens</label>
        <Placeholder name="featured_image_id" />
        
        {/*<Input type="file" name="images" />*/}

        <label>Título do Prêmio</label>
        <Input type="text" name="title" placeholder="Prêmio" />
        <label>Local</label>
        <Input type="text" name="local" placeholder="Local" />
        <label>Quantidade de Pontos</label>
        <Input type="number" name="number_points" placeholder="Quantidade de Pontos" />
        <label>Descrição do Prêmio</label>
        <Input type="text" name="description" placeholder="Descrição" />
        <label>Site do Parceiro</label>
        <Input type="text" name="site" placeholder="https://sitedoparceiro.com.br" />
        
        <button type="submit">Incluir Prêmio</button>
      </Form>
    </Container>
  );
}
