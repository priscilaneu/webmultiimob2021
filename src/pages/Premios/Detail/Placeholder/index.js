import React, { useState, useRef, useEffect } from 'react';

import { useField } from '@rocketseat/unform';
import api from '../../../../services/api';

import { Container } from './styles';

import DefaultPlaceholder from '../../../../assets/placeholder.jpg';

export default function Placeholder() {
    const { defaultValue, registerField } = useField('featured_image');

    const [file, setFile] = useState(defaultValue && defaultValue.id);
    const [preview, setPreview] = useState(defaultValue && defaultValue.url);

    const ref = useRef();

    useEffect(() => {
        if(ref.current){
            registerField({
                name: 'featured_image_id',
                ref: ref.current,
                path: 'dataset.file',
            });
        }
    }, [ref, registerField]);

    async function handleChange(e){
        const data = new FormData();

        data.append('file', e.target.files[0]);

        const response = await api.post('files', data);

        const { id, url } = response.data;

        setFile(id);
        setPreview(url);
    }

    return (
        <Container>
            <label htmlFor="featured_image">
                <img src={preview || DefaultPlaceholder} alt="" />

                <input 
                    type="file" 
                    id="featured_image" 
                    accept="image/*" 
                    data-file={file} 
                    onChange={handleChange} 
                    ref={ref}
                />
            </label>
        </Container>
    );
}
