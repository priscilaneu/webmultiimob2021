import styled from 'styled-components';

export const Container = styled.div`
  align-self: left;
  margin-bottom: 30px;
  
  label{
      cursor: pointer;

      &:hover{
          opacity: 0.7;
      }

      img{
          height: 220px;
          width: 220px;
          border: 3px solid #ccc;
          background: #eee;
      }

      input{
          display: none;
      }
  }
`;
