import React from 'react';
// import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import { MdChevronLeft } from 'react-icons/md';

import { Form, Input } from '@rocketseat/unform';

import { updateAwardRequest } from '../../../store/modules/award/actions';
import { useDispatch, useSelector } from 'react-redux';

import { Container, TopHeader } from './styles';
import Placeholder from './Placeholder';

export default function Detail() {
    const dispatch = useDispatch();
    const award = useSelector(state => state.award.award);


  function handleSubmit(data){
     dispatch(updateAwardRequest(data));
  }

  return (
    <Container>
      <TopHeader>
          <Link to='/premios'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
      </TopHeader>

        <header>
            <strong>Detalhes do Prêmio</strong>
        </header>
      <Form initialData={award} onSubmit={handleSubmit}>

        <label>Imagem de Destaque</label>
        <Placeholder name="featured_image_id" />

          {/*<label>Mais Imagens</label>
        <Input type="file" name="images" />*/}
        
        <Input type="text" name="id" placeholder="Id" style={{display: 'none'}} />
        <label>Título do Prêmio</label>
        <Input type="text" name="title" placeholder="Prêmio" />
        <label>Local</label>
        <Input type="text" name="local" placeholder="Local" />
        <label>Quantidade de Pontos</label>
        <Input type="number" name="number_points" placeholder="Quantidade de Pontos" />
        <label>Descrição do Prêmio</label>
        <Input type="text" name="description" placeholder="Descrição" />
        <label>Site do Parceiro</label>
        <Input type="text" name="site" placeholder="https://sitedoparceiro.com.br" />
          
          <button type="submit">Atualizar</button>
        </Form>
        <button type="button">Excluir</button>

    </Container>
  );
}
