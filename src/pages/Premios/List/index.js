import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import api from '../../../services/api';

import DefaultPlaceholder from '../../../assets/placeholder.jpg';
import { Container, TopHeader, Card, Left, Right } from './styles';
import { useDispatch } from 'react-redux';
import { signUpDetail } from '../../../store/modules/award/actions';

export default function List() {
    const [awards, setAwards] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        async function listAwards() {
            const response = await api.get('awards');

            const data = response.data.map(award => ({
                ...award
            }))
            setAwards(data);
        }
        listAwards();
    }, []);

    function handleDetail(data) {
        dispatch(signUpDetail(data));
    }
    return (
        <Container>
            <TopHeader>
                <Link to='/premio'>+ NOVA PREMIAÇÃO</Link>
            </TopHeader>

            <header>
                <strong>Prêmios</strong>
            </header>

            <ul>
                {awards.map(award => (
                    <Link key={award.id} to='/detalhedopremio' onClick={() => handleDetail(award)}>
                        <Card>
                            <Left>
                                <img src={(award.featured_image && award.featured_image.url) || DefaultPlaceholder} alt="Imagem Principal" />
                            </Left>
                            <Right>
                                <strong>{award.title}</strong>
                                <span>{award.description}</span>
                            </Right>
                        </Card>
                    </Link>
                ))}
            </ul>
        </Container>
    );
}
