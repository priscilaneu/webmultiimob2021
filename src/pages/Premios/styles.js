import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
    max-width: 600px;
    margin: 25px auto;
    padding-bottom: 35px;


    header{
        display: flex;
        align-self: flex-start;
        align-items: flex-start;
        text-align: flex-start;
  
        strong{
            color: #273b80;
            font-size: 24px;
            margin: 0;
        }
    }

    form{
        display:flex;
        flex-direction: column;
        margin-top: 30px;

        input{
            background: rgba(255, 255, 255, 0.8);
            border: 0;
            border-radius: 4px;
            height: 44px;
            padding: 0 15px;
            color: #666;
            margin: 0 0 10px;

            &::placeholder{
                color: #ccc;
            }
        }

        input[type='file']{
            background: transparent;
            padding: 0;
        }

        label{
            margin: 10px 0;
        }

        hr{
            border: 0;
            height: 1px;
            margin: 10px 0 20px;
            background: #ededed;
        }

        span{
            color: #f64c75;
            align-self: flex-start;
        }
        button{
            margin: 5px 0 0;
            height: 40px;
            background: #3b9eff;
            font-weight: bold;
            color: #fff;
            border: 0;
            border-radius: 4px;
            font-size: 16px;
            transition: background 0.2s;
            margint-bottom: 35px;

            &:hover{
                background: ${darken(0.03, '#3b9eff')}
            }
        }
    }
`;

export const TopHeader = styled.div`
    display: flex;
    align-self: flex-start;
    align-items: flex-start;

    a{
        display: flex;
        align-items: center;
        margin: 5px 0 60px;
        height: 40px;
        padding: 0 15px 0 2px;
        background: #3b9eff;
        font-weight: bold;
        color: #fff;
        border: 0;
        border-radius: 4px;
        font-size: 16px;
        transition: background 0.2s;

        &:hover{
        background: ${darken(0.03, '#3b9eff')}
        }
    }
`;