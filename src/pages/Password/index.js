import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';

import logo from '../../assets/logo.png';
// import { Container } from './styles';

import { signUpRequest } from '../../store/modules/auth/actions';

const schema = Yup.object().shape({
  email: Yup.string()
    .email('Insira um e-mail válido')
    .required('O e-mail é obrigatório'),
})

export default function Password() {
  const dispatch = useDispatch();

  function handleSubmit(email){
    dispatch(signUpRequest(email))
  }

  return (
    <>
      <img src={logo} alt="MultiImob" />

      <Form onSubmit={handleSubmit}>
        <Input name="email" type="email" placeholder="Informe seu e-mail" />
        
        <button type="submit">Recuperar Senha</button>
        <Link to="/">Fazer login</Link>
      </Form>
    </>
  );
}
