import React from 'react';
import { useDispatch } from 'react-redux';
import { Form, Input } from '@rocketseat/unform';
import { Link } from 'react-router-dom';

import { MdChevronLeft } from 'react-icons/md';

import { signUpRequest } from '../../store/modules/auth/actions';
import * as Yup from 'yup';

// import Avatar from './Avatar';
import { Container, TopHeader } from './styles';

export default function NewBrokers() {
  const dispatch = useDispatch();

  const schema = Yup.object().shape({
    name: Yup.string().required('O nome é obrigatório'),
    email: Yup.string().email('Insira um e-mail válido').required('O e-mail é obrigatório'),
    phone: Yup.string().required('Informe um telefone'),
  })

  function handleSubmit({name, email, password, phone, doc, obs}){
    dispatch(signUpRequest(name, email, password, phone, doc, obs));
  }

  return (
    <Container>
        <TopHeader>
            <Link to='/corretores'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
        </TopHeader>

        <header>
          <strong>Cadastrar Novo Corretor</strong>
        </header>

      <Form onSubmit={handleSubmit}>
      {/* <Form> */}

        {/* <Avatar name="avatar_id" /> */}
        
        <Input type="text" name="name" placeholder="Nome" />
        <Input type="email" name="email" placeholder="E-mail" />
        <Input name="phone" placeholder="Telefone" />
        <Input name="doc" placeholder="CRECI" />
        <Input name="obs" placeholder="Observações" />
        
        {/* <Input type="password" name="oldPassword" placeholder="Senha Atual" /> */}
        <Input type="password" name="password" placeholder="Senha" />
        {/* <Input type="password" name="confirmPassword" placeholder="Confirmar Senha" /> */}
        
        <button type="submit">Cadastrar</button>
      </Form>
    </Container>
  );
}
