import React, { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

import api from '../../../services/api';

import { Container, TopHeader, Card, Left, Right } from './styles';
import { useDispatch } from 'react-redux';
import { signUpDetail } from '../../../store/modules/corretor/actions';

export default function List() {
    const [corretores, setCorretores] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        async function listCorretores() {
            const response = await api.get('corretores');

            const data = response.data.map(corretor => ({
                ...corretor
            }))
            setCorretores(data);
        }
        listCorretores();
    }, []);

    function handleDetail(data) {
        dispatch(signUpDetail(data));
    }

    return (
        <Container>
            <TopHeader>
                <Link to='/corretor'>+ NOVO CORRETOR</Link>
            </TopHeader>

            <header>
                <strong>Corretores</strong>
            </header>

            <ul>
                {corretores.map(corretor => (
                    <Link key={corretor.id} to='/detalhedocorretor' onClick={() => handleDetail(corretor)}>
                        <Card>
                            <Right>
                                <strong>{corretor.name}</strong>
                                <span>{corretor.phone}</span>
                                <span>{corretor.email}</span>
                            </Right>
                        </Card>
                    </Link>
                ))}
                {/* <Link to='/detalhedocorretor'>
                <Card>
                    <Left>
                        <img src={DefaultUser} alt="Priscila Ribeiro"/>
                    </Left>
                    <Right>
                        <strong>Priscila Neu</strong>
                        <span>Fone: 51 9 9999-9999</span>
                    </Right>
                </Card>
            </Link> */}
            </ul>
        </Container>
    );
}
