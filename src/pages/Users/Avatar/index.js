import React, { useState, useRef, useEffect } from 'react';
import { useField } from '@rocketseat/unform';

import api from '../../../services/api';

import DefaultUser from '../../../assets/default-user-icon.png';
import { Container } from './styles';
import { uploadAvatarRequest } from '../../../store/modules/user/actions';

export default function Avatar() {
    const { defaultValue, registerField } = useField('avatar');
    
    const [file, setFile] = useState(defaultValue && defaultValue.id);
    const [preview, setPreview] = useState(defaultValue && defaultValue.url);

    const ref = useRef();

    const dispatch = useDispatch();

    useEffect(() => {
        if(ref.current) {
            registerField({
                name: 'avatar_id',
                ref: ref.current,
                path: 'dataset.file',
            })
        }
    }, [ref]);

    async function handleChange(e){
        const response = dispatch(uploadAvatarRequest(e));
        const { id, url} = response;

        setFile(id);
        setPreview(url);
    }

    return (
        <Container>
            <label htmlFor="avatar">
                <img src={ preview || DefaultUser } alt="" />

                <input 
                    type="file"
                    id="avatar"
                    accept="image/*"
                    data-file={file}
                    onChange={handleChange}
                    ref={ref}
                />
            </label>
        </Container>
    );
}
