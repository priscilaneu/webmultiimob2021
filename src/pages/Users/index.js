import React from 'react';
import { useDispatch } from 'react-redux';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';

import { Link } from 'react-router-dom';

import { signUpRequestUser } from '../../store/modules/auth/actions';

import { MdChevronLeft } from 'react-icons/md';

// import Avatar from './Avatar';
import { Container, TopHeader } from './styles';

export default function NewUsers() {
  const dispatch = useDispatch();

  const schema = Yup.object().shape({
    name: Yup.string().required('O nome é obrigatório'),
    email: Yup.string().email('Insira um e-mail válido').required('O e-mail é obrigatório'),
    phone: Yup.string().required('Informe um telefone'),
    password: Yup.string().required('A senha é obrigatória'),
  })

  function handleSubmit({name, email, password, phone, doc, obs}){
    dispatch(signUpRequestUser(name, email, password, phone, doc, obs));
  }

  return (
    <Container>
      <TopHeader>
          <Link to='/users'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
      </TopHeader>

      <header>
          <strong>Cadastrar novo Usuário</strong>
      </header>
      
      <Form schema={schema} onSubmit={handleSubmit}>
      
        {/* <Avatar name="avatar_id" /> */}
        
        <Input type="text" name="name" placeholder="Nome" />
        <Input type="email" name="email" placeholder="E-mail" />
        <Input name="phone" placeholder="Telefone" />
        {/* <Input name="doc" placeholder="CPF" /> */}
        {/* <Input name="obs" placeholder="Observações" /> */}

        <hr />

        <Input type="password" name="password" placeholder="Senha" />
        {/* <Input type="password" name="confirmPassword" placeholder="Confirmar Senha" />  */}
        
        <button type="submit">Cadastrar Usuário</button>
      </Form>
    </Container>
  );
}
