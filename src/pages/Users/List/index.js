import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import api from '../../../services/api';

import DefaultUser from '../../../assets/default-user-icon.png';
import { Container, TopHeader, Card, Left, Right } from './styles';
import { useDispatch } from 'react-redux';
import { signUpDetail } from '../../../store/modules/user/actions';

export default function List() {
    const [users, setUsers] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        async function listUsers() {
            const response = await api.get('users');

            const data = response.data.map(user => ({
                ...user
            }))
            setUsers(data);
        }
        listUsers();
    }, []);


    function handleDetail(data) {
        dispatch(signUpDetail(data));
    }

    return (
        <Container>
            <TopHeader>
                <Link to='/user'>+ NOVO USUÁRIO</Link>
            </TopHeader>

            <header>
                <strong>Usuários</strong>
            </header>

            <ul>
                {users.map(user => (
                    <Link key={user.id} to='/detalhedousuario' onClick={() => handleDetail(user)}>
                        <Card>
                            <Right>
                                <strong>{user.name}</strong>
                                <span>{user.phone}</span>
                                <span>{user.email}</span>
                            </Right>
                        </Card>
                    </Link>
                ))}
                {/* <Link to='/detalhedousuario'>
                        <Card>
                            <Left>
                                <img src={DefaultUser} alt="Priscila Ribeiro"/>
                            </Left>
                            <Right>
                                <strong>Priscila Neu</strong>
                                <span>Fone: 51 9 9999-9999</span>
                            </Right>
                        </Card>
                    </Link> */}
            </ul>
        </Container>
    );
}
