import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
  max-width: 600px;
  margin: 25px auto;
  padding-bottom: 35px;

  display: flex;
  flex-direction: column;

  header{
      display: flex;
      align-self: flex-start;
      align-items: flex-start;

      strong{
          color: #273b80;
          font-size: 24px;
          margin: 0 15px;
      }
  }

  ul{
      display: grid;
      grid-template-columns: repeat(1, 1fr);
      grid-gap: 15px;
      margin-top: 30px;
  }
`;

export const TopHeader = styled.div`
    display: flex;
    align-self: flex-start;
    align-items: flex-start;

    a{
        display: flex;
        align-items: center;
        margin: 5px 0 60px;
        height: 40px;
        padding: 0 15px;
        background: #3b9eff;
        font-weight: bold;
        color: #fff;
        border: 0;
        border-radius: 4px;
        font-size: 16px;
        transition: background 0.2s;

        &:hover{
        background: ${darken(0.03, '#3b9eff')}
        }
    }
`
export const Card = styled.li`
  display: inline-block;
  padding: 20px;
  border-radius: 4px;
  background: #fff;
  width: 100%;
`;

export const Left = styled.div`
    display: inline-block;
    
    img{
        width: 48px;
        height: 48px;
        border-radius: 50%;
    }
`;

export const Right = styled.div`
    display: inline-block;
    padding-left: 15px;
    strong{
        display: block;
        color: #273b80;
        font-size: 20px;
        font-weight: normal;
    }

    span{
        display: block;
        margin-top: 3px;
        color: #666;
    }
`;