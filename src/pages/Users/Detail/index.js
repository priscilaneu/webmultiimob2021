import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import { MdChevronLeft } from 'react-icons/md';

import { Form, Input } from '@rocketseat/unform';

import { updateUserRequest } from '../../../store/modules/user/actions';

// import Avatar from './Avatar';
import { Container, TopHeader } from './styles';

export default function Detail() {
   const dispatch = useDispatch();
   const user = useSelector(state => state.user.useredit);


  function handleSubmit(data){
     dispatch(updateUserRequest(data));
  }

  return (
    <Container>
      <TopHeader>
          <Link to='/users'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
      </TopHeader>

        <header>
            <strong>Detalhes do Usuário</strong>
        </header>
        <Form initialData={user} onSubmit={handleSubmit}>

          {/* <Avatar name="avatar_id" /> */}

          <Input type="text" name="id" placeholder="Id" style={{display: 'none'}} />
          <Input type="text" name="name" placeholder="Nome" />
          <Input type="email" name="email" placeholder="E-mail" />
          <Input name="phone" placeholder="Telefone" />
          <Input name="doc" placeholder="CPF" />
          <Input name="obs" placeholder="Observações" />
          
          <button type="submit">Atualizar</button>
        </Form>

    </Container>
  );
}
