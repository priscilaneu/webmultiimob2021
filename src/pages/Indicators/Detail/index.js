import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import { MdChevronLeft } from 'react-icons/md';

import { Form, Input, Textarea } from '@rocketseat/unform';

import { updateRequest } from '../../../store/modules/recommendation/actions';

import { Container, TopHeader } from './styles';
import Select from "../../../components/Select";

export default function Detail() {
   const dispatch = useDispatch();
   const recommendation = useSelector(state => state.recommendation.recommendation);


  function handleSubmit(data){
     dispatch(updateRequest(data));
  }

  return (
    <Container>
      <TopHeader>
          <Link to='/indicators'><MdChevronLeft size={36} color="#FFF"/> VOLTAR</Link>
      </TopHeader>

        <header>
            <strong>Detalhes da Indicação</strong>
        </header>
      <Form initialData={recommendation} onSubmit={handleSubmit}>

          {/* <Avatar name="avatar_id" /> */}
          <label>Cód da Indicação</label>
          <Input type="number" readOnly={true} name="id" placeholder="Código da Indicação" />
          <label>Status do Imóvel</label>
          <Select name="status" options={[{ label: "Sem negociação", value: 'Semnegociacao' },
              { label: "Em negociação", value: 'Negociacao' },
              { label: "Vendido", value: 'Vendido' }]}>
          </Select>
          <label>Indicação de Compra ou Venda?</label>
          <Select name="recommendation_type"  options={[{ label: "Compra", value: 'Compra' },
              { label: "Venda", value: 'Venda' }]}>
          </Select>
          <label>Total de Pontos</label>
          <Input type="number" name="number_points" placeholder="Pontuação" />
          <label>Contato do Imóvel</label>
          <Input type="text" name="name" placeholder="Contato do Imóvel" />
          <label>Telefone do Contato</label>
          <Input name="phone" placeholder="Telefone do Contato" />
          <label>E-mail do Contato</label>
          <Input name="email" placeholder="E-mail do Contato" />
          <label>Corretor</label>
          <Input name="broker_id" style={{display: 'none'}} />
          <Input name="broker.name" placeholder="Nome do Corretor" />
          <label>Observações</label>
          <Textarea name="obs" rows="4"></Textarea>
          
          <button type="submit">Atualizar</button>
        </Form>
        <button type="button">Excluir</button>

    </Container>
  );
}
