import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

import { Container, Card, Left, Right } from './styles';
import {useDispatch} from "react-redux";
import api from "../../../services/api";
import {signUpDetail} from "../../../store/modules/recommendation/actions";

export default function List() {

    const [recommendations, setRecommendations] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        async function listRecommendations() {
            const response = await api.get('recommendations');

            const data = response.data.map(recommendation => ({
                ...recommendation
            }))
            setRecommendations(data);
        }
        listRecommendations();
    }, []);

    function handleDetail(data) {
        dispatch(signUpDetail(data));
    }

  return (
    <Container>
        <header>
            <strong>Indicações</strong>
        </header>

        <ul>
            {recommendations.map(recommendation => (
            <Link key={recommendation.id} to="/indicator" onClick={() => handleDetail(recommendation)}>
                <Card>
                    <Left>
                        <strong>CONTATO: {recommendation.name}</strong>
                        <span>Telefone: {recommendation.phone}</span>
                        <span>Corretor: {recommendation.broker.name}</span>
                    </Left>
                    <Right>
                        
                    </Right>
                </Card>
            </Link>
            ))}
        </ul>
    </Container>
  );
}
