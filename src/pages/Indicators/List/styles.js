import styled from 'styled-components';

export const Container = styled.div`
  max-width: 600px;
  margin: 50px auto;
  padding-bottom: 35px;

  display: flex;
  flex-direction: column;

  header{
      display: flex;
      align-self: flex-start;
      align-items: flex-start;

      strong{
          color: #273b80;
          font-size: 24px;
          margin: 0;
      }
  }

  ul{
      display: grid;
      grid-template-columns: repeat(1, 1fr);
      grid-gap: 15px;
      margin-top: 30px;
  }
`;

export const Card = styled.li`
  display: inline-block;
  padding: 20px;
  border-radius: 4px;
  background: #fff;
  width: 100%;
`;

export const Left = styled.div`
    display: inline-block;
    
    strong{
        display: block;
        color: #273b80;
        font-size: 20px;
        font-weight: normal;
    }

    span{
        display: block;
        margin-top: 3px;
        color: #666;
    }
`;

export const Right = styled.div`
    display: inline-block;
    padding-left: 15px;
    strong{
        display: block;
        color: #273b80;
        font-size: 20px;
        font-weight: normal;
    }

    span{
        display: block;
        margin-top: 3px;
        color: #666;
    }
`;