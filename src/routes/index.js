import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '../pages/SignIn';
import Password from '../pages/Password';

import Dashboard from '../pages/Dashboard';
import Indicator from '../pages/Indicators/Detail';
import Indicators from '../pages/Indicators/List';
import Premio from '../pages/Premios';
import Premios from '../pages/Premios/List';
import DetalhePremio from '../pages/Premios/Detail';
import Profile from '../pages/Profile';
import Users from '../pages/Users/List';
import User from '../pages/Users';
import DetailUser from '../pages/Users/Detail';
import Clients from '../pages/Clients/List';
import Client from '../pages/Clients';
import DetailClient from '../pages/Clients/Detail';

import Brokers from '../pages/Brokers/List';
import Broker from '../pages/Brokers';
import DetailBroker from '../pages/Brokers/Detail';


export default function Routes(){
    return(
        <Switch>
            <Route path="/" exact component={SignIn} />
            <Route path="/password" exact component={Password} />

            <Route path="/dashboard" exact component={Dashboard} isPrivate />
            <Route path="/indicators" exact component={Indicators} isPrivate />
            <Route path="/indicator" exact component={Indicator} isPrivate />
            <Route path="/premios" exact component={Premios} isPrivate />
            <Route path="/premio" exact component={Premio} isPrivate />
            <Route path="/detalhedopremio" exact component={DetalhePremio} isPrivate />
            <Route path="/profile" exact component={Profile} isPrivate />
            <Route path="/users" exact component={Users} isPrivate />
            <Route path="/user" exact component={User} isPrivate />
            <Route path="/detalhedousuario" exact component={DetailUser} isPrivate />
            <Route path="/clientes" exact component={Clients} isPrivate />
            <Route path="/cliente" exact component={Client} isPrivate />
            <Route path="/detalhedocliente" exact component={DetailClient} isPrivate />
            <Route path="/corretores" exact component={Brokers} isPrivate />
            <Route path="/corretor" exact component={Broker} isPrivate />
            <Route path="/detalhedocorretor" exact component={DetailBroker} isPrivate />
        </Switch>
    )
}