import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '../../../services/api';
import history from '../../../services/history';

import { signInSuccess, signFailure } from './actions';

export function* signIn({ payload }){
    try{
        const { email, password } = payload;

        const response = yield call(api.post, 'sessions', {
            email,
            password
        });

        const { token, user } = response.data;

        if(!user.active){
            toast.error('Usuário não está ativo');
            return;
        }

        api.defaults.headers.Authorization = `Bearer ${token}`;

        yield put(signInSuccess(token, user));

        history.push('/dashboard');
    }catch(err){
        toast.error('Falha na autenticação, verifique seus dados!');
        console.log(err);
        yield put(signFailure());
    }   
}

export function* signUp({ payload }) {
    try{
        const { name, email, phone, password, doc, obs } = payload;

        yield call(api.post, 'corretor', {
            name, email, phone, password, doc, obs
        });

        // // const token = response.data;
        // yield put(signInSuccess(token));

        toast.success('Corretor cadastrado com sucesso!');
        // // yield put(signUpRequest(response.data));
        history.push('/corretores');
    }catch(err){
        toast.error('Falha no cadastro, verifique seus dados!');

        yield put(signFailure());
    }
}

export function* signUpUser({ payload }) {
    try{
        const { name, email, phone, password, doc, obs } = payload;

        yield call(api.post, 'user', {
            name, email, phone, password, doc, obs
        });

        // // const token = response.data;
        // yield put(signInSuccess(token));

        toast.success('Novo usuário cadastrado com sucesso!');
        // // yield put(signUpRequest(response.data));
        history.push('/users');
    }catch(err){
        toast.error('Falha no cadastro, verifique os dados!');

        yield put(signFailure());
    }
}

export function setToken({ payload }){
    if(!payload) return;

    const { token } = payload.auth;

    if(token){
        api.defaults.headers.Authorization = `Bearer ${token}`;
    }
}

export function signOut(){
    history.push('/');
}

export default all([
    takeLatest('persist/REHYDRATE', setToken),
    takeLatest('@auth/SIGN_IN_REQUEST', signIn),
    takeLatest('@auth/SIGN_UP_REQUEST', signUp),
    takeLatest('@auth/SIGN_UP_REQUEST_USER', signUpUser),
    takeLatest('@auth/SIGN_OUT', signOut),
]);