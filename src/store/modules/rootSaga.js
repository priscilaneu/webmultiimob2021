import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import user from './user/sagas';
import broker from './corretor/sagas';
import client from './cliente/sagas';
import award from './award/sagas';
import recommendation from './recommendation/sagas';

export default function* rootSaga(){
    return yield all([auth, user, broker, client, award, recommendation]);
}