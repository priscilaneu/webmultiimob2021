import produce from 'immer';

const INITIAL_STATE = {
    broker: null,
};

export default function broker(state = INITIAL_STATE, action){
    return produce(state, draft => {
        switch(action.type) {
            case '@broker/SIGN_UP_DETAIL': {
                draft.broker = action.payload.broker;
                break;
            }
            case '@broker/UPDATE_BROKER_REQUEST': {
                break;
            }
            case '@broker/UPDATE_BROKER_SUCCESS': {
                break;
            }
            case '@broker/UPDATE_BROKER_FAILURE': {
                break;
            }
            default:
        }
    });
}