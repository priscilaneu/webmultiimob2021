export function signUpDetail(broker){
    return {
        type: '@broker/SIGN_UP_DETAIL',
        payload: { broker },
    };
}

export function updateBrokerRequest(data){
    return{
        type: '@broker/UPDATE_BROKER_REQUEST',
        payload: { data },
    }
}

export function updateBrokerSuccess(broker){
    return{
        type: '@broker/UPDATE_BROKER_SUCCESS',
        payload: { broker },
    }
}

export function updateBrokerFailure(){
    return{
        type: '@broker/UPDATE_BROKER_FAILURE',
    }
}