import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import history from '../../../services/history';
import { updateBrokerSuccess, updateBrokerFailure } from './actions';
import api from '../../../services/api';

export function signUpDetail() {
    history.push('/detalhedocorretor');
}

export function* updateBroker({ payload }){
    try{
        const { id, name, email, phone, doc, obs, avatar_id, ...rest } = payload.data;

        const broker = Object.assign({ id, name, email, phone, doc, obs, avatar_id }, rest.oldPassword ? rest : {});

        const response = yield call(api.put, 'corretor', broker);

        toast.success('Corretor atualizado com sucesso!');

        yield put(updateBrokerSuccess(response.data));
    }catch(err){
        toast.error('Erro ao atualizar o corretor, confira seus dados!');
        yield put(updateBrokerFailure());
    }
}

export default all([
    takeLatest('@broker/SIGN_UP_DETAIL', signUpDetail),
    takeLatest('@broker/UPDATE_BROKER_REQUEST', updateBroker),
]);