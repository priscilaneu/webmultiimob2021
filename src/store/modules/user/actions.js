export function updateProfileRequest(data){
    return{
        type: '@user/UPDATE_PROFILE_REQUEST',
        payload: { data },
    }
}

export function updateProfileSuccess(profile){
    return{
        type: '@user/UPDATE_PROFILE_SUCCESS',
        payload: { profile },
    }
}

export function updateProfileFailure(){
    return{
        type: '@user/UPDATE_PROFILE_FAILURE',
    }
}

export function updateUserRequest(data){
    return{
        type: '@user/UPDATE_USER_REQUEST',
        payload: { data },
    }
}

export function updateUserSuccess(profile){
    return{
        type: '@user/UPDATE_USER_SUCCESS',
        payload: { profile },
    }
}

export function updateUserFailure(){
    return{
        type: '@user/UPDATE_USER_FAILURE',
    }
}

// export function uploadAvatarRequest(data){
//     return {
//         type: '@user/UPLOAD_AVATAR',
//         payload: { data },
//     };
// }

export function signUpDetail(user){
    return {
        type: '@user/SIGN_UP_DETAIL',
        payload: { user },
    };
}

// export function uploadAvatarSuccess(data){
//     return{
//         type: '@user/UPLOAD_AVATAR_SUCCESS',
//         payload: { data },
//     }
// }

// export function uploadAvatarFailure(){
//     return{
//         type: '@user/UPLOAD_AVATAR_FAILURE',
//     }
// }