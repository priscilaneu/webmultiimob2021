import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '../../../services/api';

import { updateProfileSuccess, updateProfileFailure, updateUserSuccess, updateUserFailure } from './actions';
import history from '../../../services/history';

export function* updateProfile({ payload }){
    try{
        const { id, name, email, phone, doc, obs, ...rest } = payload.data;

        const profile = Object.assign({ id, name, email, phone, doc, obs }, rest.oldPassword ? rest : {});

        const response = yield call(api.put, 'user', profile);

        toast.success('Perfil atualizado com sucesso!');

        yield put(updateProfileSuccess(response.data));
    }catch(err){
        toast.error('Erro ao atualizar perfil, confira seus dados!');
        yield put(updateProfileFailure());
    }
}

// export function* uploadFile({ payload }) {
//     try{
//         const data = new FormData();
        
//         data.append('file', payload.target.files[0]);
//         const response = yield call(api.post, 'files', data);
//         toast.success('Avatar carregado com sucesso! Clique em Atualizar Perfil');
//         yield put(uploadAvatarSuccess(response.data));
//     }catch(err){
//         toast.error('Falha no cadastro, verifique seus dados!');

//         yield put(uploadAvatarFailure());
//     }
// }

export function* updateUser({ payload }){
    try{
        const { id, name, email, phone, doc, obs, avatar_id, ...rest } = payload.data;

        const user = Object.assign({ id, name, email, phone, doc, obs, avatar_id }, rest.oldPassword ? rest : {});

        const response = yield call(api.put, 'user', user);

        toast.success('Usuário atualizado com sucesso!');

        yield put(updateUserSuccess(response.data));
    }catch(err){
        toast.error('Erro ao atualizar o usuário, confira seus dados!');
        yield put(updateUserFailure());
    }
}

export function signUpDetail() {
    history.push('/detalhedousuario');
}

export default all([
    takeLatest('@user/SIGN_UP_DETAIL', signUpDetail),
    takeLatest('@user/UPDATE_PROFILE_REQUEST', updateProfile),
    takeLatest('@user/UPDATE_USER_REQUEST', updateUser),
    // takeLatest('@auth/UPLOAD_AVATAR', uploadFile),
])