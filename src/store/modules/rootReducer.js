import { combineReducers } from 'redux';

import auth from './auth/reducer';
import user from './user/reducer';
import broker from './corretor/reducer'
import client from './cliente/reducer'
import award from './award/reducer'
import recommendation from './recommendation/reducer'

export default combineReducers({
    auth,
    user,
    broker,
    client,
    award,
    recommendation
})