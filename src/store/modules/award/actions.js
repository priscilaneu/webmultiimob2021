export function signUpDetail(award){
    return {
        type: '@award/SIGN_UP_DETAIL',
        payload: { award },
    };
}

export function signUpRequest(data){
    return{
        type: '@award/SIGN_UP_REQUEST',
        payload: { data },
    }
}

export function updateAwardRequest(data){
    return{
        type: '@award/UPDATE_AWARD_REQUEST',
        payload: { data },
    }
}

export function updateAwardSuccess(award){
    return{
        type: '@award/UPDATE_AWARD_SUCCESS',
        payload: { award },
    }
}

export function updateAwardFailure(){
    return{
        type: '@award/UPDATE_AWARD_FAILURE',
    }
}

export function signUpFailure(){
    return {
        type: '@award/SIGN_UP_FAILURE',
    }
}