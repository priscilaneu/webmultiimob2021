import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import history from '../../../services/history';
import { updateAwardSuccess, updateAwardFailure, signUpFailure } from './actions';
import api from '../../../services/api';

export function signUpDetail() {
    history.push('/detalhedopremio');
}

export function* signUp({ payload }) {
    try{
        const { title, local, number_points, description, site, featured_image_id } = payload.data;

        const award = Object.assign({ title, local, number_points, description, site, featured_image_id });

        yield call(api.post, 'award', award);

        toast.success('Premiação cadastrada com sucesso!');

        history.push('/premios');
    }catch(err){
        toast.error('Falha no cadastro, verifique seus dados!');

        yield put(signUpFailure());
    }
}

export function* updateAward({ payload }){
    try{
        const { id, title, local, number_points, description, site, featured_image_id } = payload.data;

        const award = Object.assign({ id, title, local, number_points, description, site, featured_image_id });

        const response = yield call(api.put, 'award', award);

        toast.success('Premiação atualizado com sucesso!');

        yield put(updateAwardSuccess(response.data));
    }catch(err){
        toast.error('Erro ao atualizar a premiação, confira seus dados!');
        yield put(updateAwardFailure());
    }
}

export default all([
    takeLatest('@award/SIGN_UP_DETAIL', signUpDetail),
    takeLatest('@award/UPDATE_AWARD_REQUEST', updateAward),
    takeLatest('@award/SIGN_UP_REQUEST', signUp),
]);