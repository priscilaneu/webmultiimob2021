import produce from 'immer';

const INITIAL_STATE = {
    award: null,
};

export default function award(state = INITIAL_STATE, action){
    return produce(state, draft => {
        switch(action.type) {
            case '@award/SIGN_UP_DETAIL': {
                draft.award = action.payload.award;
                break;
            }
            case '@award/SIGN_UP_REQUEST': {
                draft.token = action.payload.token;
                draft.loading = false;
                draft.signed = true;
                break;
            }
            case '@award/UPDATE_AWARD_REQUEST': {
                break;
            }
            case '@award/UPDATE_AWARD_SUCCESS': {
                break;
            }
            case '@award/UPDATE_AWARD_FAILURE': {
                break;
            }
            default:
        }
    });
}