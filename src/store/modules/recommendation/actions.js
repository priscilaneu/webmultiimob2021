export function signUpDetail(recommendation){
    return {
        type: '@recommendation/SIGN_UP_DETAIL',
        payload: { recommendation },
    };
}

export function updateRequest(data){
    return{
        type: '@recommendation/UPDATE_RECOMMENDATION_REQUEST',
        payload: { data },
    }
}

export function updateSuccess(recommendation){
    return{
        type: '@recommendation/UPDATE_RECOMMENDATION_SUCCESS',
        payload: { recommendation },
    }
}

export function updateFailure(){
    return{
        type: '@award/UPDATE_RECOMMENDATION_FAILURE',
    }
}