import produce from 'immer';

const INITIAL_STATE = {
    recommendation: null,
};

export default function recommendation(state = INITIAL_STATE, action){
    return produce(state, draft => {
        switch(action.type) {
            case '@recommendation/SIGN_UP_DETAIL': {
                draft.recommendation = action.payload.recommendation;
                break;
            }
            case '@recommendation/UPDATE_RECOMMENDATION_REQUEST': {
                break;
            }
            case '@recommendation/UPDATE_RECOMMENDATION_SUCCESS': {
                break;
            }
            case '@recommendation/UPDATE_RECOMMENDATION_FAILURE': {
                break;
            }
            default:
        }
    });
}