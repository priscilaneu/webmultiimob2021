import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import history from '../../../services/history';
import { updateSuccess, updateFailure } from './actions';
import api from '../../../services/api';

export function signUpDetail() {
    history.push('/indicator');
}

export function* update({ payload }){
    try{
        const { id, name, email, phone, recommendation_type, number_points, status, obs } = payload.data;
        console.log(payload.data)
        const recommendation = Object.assign({ id, name, email, phone, recommendation_type, number_points, status, obs });

        const response = yield call(api.put, 'recommendation', recommendation);

        toast.success('Indicação atualizado com sucesso!');

        yield put(updateSuccess(response.data));
    }catch(err){
        toast.error('Erro ao atualizar a indicação, confira seus dados!');
        yield put(updateFailure());
    }
}

export default all([
    takeLatest('@recommendation/SIGN_UP_DETAIL', signUpDetail),
    takeLatest('@recommendation/UPDATE_RECOMMENDATION_REQUEST', update)
]);