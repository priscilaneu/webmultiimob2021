import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import history from '../../../services/history';
import { updateClientSuccess, updateClientFailure } from './actions';
import api from '../../../services/api';

export function signUpDetail() {
    history.push('/detalhedocliente');
}

export function* updateClient({ payload }){
    try{
        const { id, name, email, phone, doc, obs, ...rest } = payload.data;

        const client = Object.assign({ id, name, email, phone, doc, obs }, rest.oldPassword ? rest : {});

        const response = yield call(api.put, 'client', client);

        toast.success('Cliente atualizado com sucesso!');

        yield put(updateClientSuccess(response.data));
    }catch(err){
        toast.error('Erro ao atualizar esse cliente, confira seus dados!');
        yield put(updateClientFailure());
    }
}

export default all([
    takeLatest('@client/SIGN_UP_DETAIL', signUpDetail),
    takeLatest('@client/UPDATE_CLIENT_REQUEST', updateClient),
]);