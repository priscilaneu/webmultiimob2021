import produce from 'immer';

const INITIAL_STATE = {
    client: null,
};

export default function client(state = INITIAL_STATE, action){
    return produce(state, draft => {
        switch(action.type) {
            case '@client/SIGN_UP_DETAIL': {
                draft.client = action.payload.client;
                break;
            }
            case '@client/UPDATE_CLIENT_REQUEST': {
                break;
            }
            case '@client/UPDATE_CLIENT_SUCCESS': {
                break;
            }
            case '@client/UPDATE_CLIENT_FAILURE': {
                break;
            }
            default:
        }
    });
}