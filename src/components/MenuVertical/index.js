import React from 'react';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo-header.png';
import { Container, SubItems} from './styles';


export default function MenuVertical() {
  return (
    <Container>
        {/* <Content> */}
            <nav>
                <img src={logo} alt="MultiImob" />
                <br/><br/>
                <Link to='/dashboard'>INÍCIO</Link>
                <Link to='/indicators'>INDICAÇÕES</Link>
                <p>PRÊMIOS</p>
                <SubItems>
                  <Link to='/premios'>Listagem de Prêmios</Link>
                  <Link to='/premio'>Incluir novo Prêmio</Link>
                </SubItems>
                <p>CLIENTES</p>
                <SubItems>
                  <Link to='/clientes'>Listagem de Clientes</Link>
                  <Link to='/cliente'>Incluir novo Clientes</Link>
                </SubItems>
                <p>CORRETORES</p>
                <SubItems>
                  <Link to='/corretores'>Listagem de Corretores</Link>
                  <Link to='/corretor'>Incluir novo Corretor</Link>
                </SubItems>
                <p>USUÁRIOS</p>
                <SubItems>
                  <Link to='/users'>Listagem de Usuários</Link>
                  <Link to='/user'>Incluir novo Usuário</Link>
                </SubItems>
            </nav>
        {/* </Content> */}
    </Container>
  );
}
