import styled from 'styled-components';

export const Container = styled.div`
    background: #273b80;
    height: 100vh;
    width: 280px;
    max-width: 280px;
    min-width: 280px;
    display: flex;
    justify-content: space-between;
    align-items: start;
    position: fixed;
    top: 0;

    nav{
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        z-index: 999;

        img{
            padding-top: 20px;
            margin
            padding-right: 10px;
            padding-left: 10px;
            border-bottom: 1px solid #f2f2f2;
        }

        p{
            font-weight: bold;
            color: #fff;
            padding: 15px 15px 5px 15px;
        }

        a{
            font-weight: bold;
            color: #fff;
            padding: 15px 15px 5px 15px;
        }
    }
`;

export const SubItems = styled.div`
    padding-left: 20px;
    padding-top: 0;

    a{
        display: flex;
        flex-direction: column;
        color: #fff;
        padding: 10px; 
        font-weight: normal;
    }
    
`;
