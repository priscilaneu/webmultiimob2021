import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { signOut } from '../../store/modules/auth/actions';

import DefaultUser from '../../assets/default-user-icon.png';
import MenuVertical from '../MenuVertical';

import { Container, Content, Profile } from './styles';

export default function Header(){
    const dispatch = useDispatch();
    const profile = useSelector(state => state.user.profile);

    function handleSignOut(){
        dispatch(signOut());
    }

    return (
        <>
        <MenuVertical/>

        <Container>
            <Content>
                
                {/* <nav>
                    <img src={logo} alt="MultiImob" />
                    <Link to='/dashboard'>INÍCIO</Link>
                    <Link to='/indicators'>INDICAÇÕES</Link>
                    <Link to='/premios'>PRÊMIOS</Link>
                    <Link to='/corretores'>CORRETORES</Link>
                    <Link to='/users'>USUÁRIOS</Link>
                </nav> */}
                <aside>
                    <Profile>
                        <div>
                            <strong>{profile.name}</strong>
                            <Link to="/profile">Meu Perfil</Link>
                            <button type="button" onClick={handleSignOut} >Sair</button>
                        </div>
                        <img src={profile.avatar && profile.avatar.url || DefaultUser} alt="Avatar"/>
                    </Profile>
                </aside>
            </Content>
        </Container>
        </>
    )
}


