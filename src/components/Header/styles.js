import styled from 'styled-components';

export const Container = styled.div`
    background: #fff;
    padding: 0 30px;
    box-shadow: 4px 4px 4px #ccc;
`;

export const Content = styled.div`
    height: 85px;
    max-width: 95%;
    margin: 0 auto;
    display: flex;
    justify-content: flex-end;
    align-items: center;
  
    nav{
        display: flex;
        align-items: center;

        img{
            margin-right: 20px;
            padding-right: 20px;
        }

        a{
            font-weight: bold;
            color: #fff;
            padding: 15px;
        }
    }

    aside{
        display: flex;
        align-items: center;
    }
`;

export const Profile = styled.div`
    display: flex;
    margin-left: 20px;
    padding-left: 20px;
    
    div{
        text-align: right;
        margin-right: 10px;

        strong{
            display: block;
            color: #767575;
        }

        a{
            display: block;
            margin-top: 2px;
            font-size: 12px;
            color: #a5a5a5;
        }
        
        button{
            color: #a5a5a5;
            background: transparent;
            border: none;
            font-size: 12px;
        }
    }

    img{
        width: 48px;
        height: 48px;
        border-radius: 50%;
    }
`;
