import React, { useRef, useEffect } from "react";
import ReactSelect from "react-select";
import { useField } from "@rocketseat/unform";

const Select = ({ name, options, ...rest }) => {
    const selectRef = useRef(null);
    const { fieldName, defaultValue, registerField, error } = useField(name);

    const customStyles = {
        option: (provided, state) => ({
            ...provided,
        }),
        control: () => ({
            background: 'rgba(255,255,255,0.8)',
            height: 44,
            color: '#666',
            display: 'flex',
            radius: 4
        }),
        singleValue: (provided, state) => {
            const opacity = state.isDisabled ? 0.5 : 1;
            const transition = 'opacity 300ms';

            return { ...provided, opacity, transition };
        }
    }

    useEffect(() => {
        registerField({
            name: fieldName,
            ref: selectRef.current,
            path: "state.value",
            getValue: ref => {
                if (rest.isMulti) {
                    if (!ref.state.value) {
                        return [];
                    }
                    return ref.state.value.map(option => option.value);
                } else {
                    if (!ref.state.value) {
                        return "";
                    }
                    return ref.state.value.value;
                }
            }
        });
    }, [fieldName, registerField, rest.isMulti]);

    return (
        <ReactSelect
            styles={customStyles}
            defaultValue={
                defaultValue && options.find(option => option.value === defaultValue)
            }
            ref={selectRef}
            classNamePrefix="react-select"
            options={options}
            {...rest}
        />
    );
};
export default Select;
